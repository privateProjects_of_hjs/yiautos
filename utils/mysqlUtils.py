# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月14日  10点17分
@contactInformation: 727803257@qq.com
@Function: 实现连接一个指定的mysql数据库，并执行相关操作
"""

# 请从这行往下开始编写脚本

# 细节：
# --》1.pymysql库提供的常用方法汇总：
# --》1.1.conn = pymysql.connect()     # 建立数据库连接
# --》1.2.cur  = conn.cursor()         # 创建游标
# --》1.3.cur.execute()                # 执行sql语句
# --》1.4.cur.close()                  # 关闭游标
# --》1.5.conn.commit()                # 提交事物（在向数据库新增/修改/删除数据时必须要调用这个方法，否则新增/修改/删除的sql语句不会被真正执行)
# --》1.6.conn.rollback()              # 执行sql语句发生错误时进行回滚
# --》1.7.conn.close()                 # 关闭数据库连接(每次执行完sql语句，建议最后都要调用这个方法来关闭mysql进程，因为这样可以降低系统内存)
# -->2.pymysql库基本使用的学习网址：https://zhuanlan.zhihu.com/p/34489844

import pymysql


class MysqlUtils:
    """实现连接一个指定的mysql数据库，并执行相关操作"""

    # 第1步：设置一个属性mysql_database_connection_information，用于存储mysql数据库登录账号信息
    # 细节：
    # --》1.属性mysql_database_connection_information的含义：mysql数据库登录账号信息
    # --》2.属性mysql_database_connection_information的数据类型：dict
    # --》3.属性mysql_database_connection_information的值必须只能在子类里被重写(被重写后的值必须是正确的mysql数据库登录账号信息)
    mysql_database_connection_information = {}

    # 细节：
    # --》1.方法"__init__"在父类或子类被实例化后才会被执行，且执行优先级低于父类或子类里的属性
    def __init__(self):

        # 第1步：得到一个mysql数据库连接池对象
        # 细节：
        # --》1.参数conn的含义：mysql数据库连接池对象
        # --》2.连接池对象可以理解为是一条高速公路
        # --》3.添加参数cursorclass并把参数值设置为'pymysql.cursors.DictCurso'所实现的作用：是让方法fetchall的返回值的数据类型变为一个【元祖内嵌套字典】的元祖
        # --》4.不添加参数cursorclass所实现的作用：是让方法fetchall的返回值的数据类型变为一个【元祖内嵌套列表】的元祖
        # --》5.我们习惯让方法fetchall的返回值的数据类型变为一个【元祖内嵌套字典】的元祖
        self.conn = pymysql.connect(host=self.mysql_database_connection_information['host'],
                                    port=int(self.mysql_database_connection_information['port']),
                                    user=self.mysql_database_connection_information['user'],
                                    passwd=self.mysql_database_connection_information['passwd'],
                                    db=self.mysql_database_connection_information['db'],
                                    charset=self.mysql_database_connection_information['charset'],
                                    cursorclass=pymysql.cursors.DictCursor)
        # 第2步：得到一个mysql数据库游标对象
        # 细节：
        # --》1.参数mysqlCur的含义：游标对象
        # --》2.游标对象可以理解为是在一条高速公路上行驶的一辆货车
        self.mysqlCur = self.conn.cursor()

    def query_of_sql(self, sql: str):
        """
        :param  sql  一条用于查询的sql，数据类型为str
        :return 返回值是一个【元祖内嵌套字典】的值，数据类型为元祖
                (最外层元祖的每个下标对应的值都是一个最内层的字典，每个最内层的字典的值都对应符合查询条件的其中一条表数据）

        # 细节：
        # --》1.接口作用：执行一条用于查询的sql，返回符合条件的所有表数据
        # --》2.一条用于查询的sql：可能涉及单表查询也可能涉及多表查询，总结来说任何一条用于查询的sql都能被执行
        # --》3.一条表数据里的字段值：可以是某张数据表的表字段的值，也可以是函数值比如count函数的值
        # --》4.接口返回值比如为：[{'id': '10001', 'name': '老师'},{'id': '10002', 'name': '学生'}]
        """
        try:
            # 第1步：调用pymysql库提供的execute方法来执行sql，把结果存储到对象mysqlCur
            self.mysqlCur.execute(sql)
        except Exception as errorMessage:
            print("该sql语句，在执行过程中出现问题，异常信息为：{errorMessage}".format(errorMessage=errorMessage))
        else:
            # 第2步：调用pymysql库提供的fetchall方法，通过执行存储在对象mysqlCur的sql来得到查询结果，查询结果的数据类型为list
            data = self.mysqlCur.fetchall()
            # 第3步：返回查询结果
            return data

    # 以下的所有代码都后续再进行修改
    # def commit_of_sql(self, sql: str):
    #     """
    #     :param  sql  一条新增/修改/删除的sql,数据类型为str
    #
    #     # 细节：
    #     # --》1.接口作用：执行一条新增/修改/删除这3种类型里的其中1种类型的sql
    #     """
    #     try:
    #         # 第1步：调用pymysql库提供的execute方法来执行sql，把sql存储到对象mysqlCur
    #         self.mysqlCur.execute(sql)
    #     except Exception as errorMessage:
    #         # 细节：
    #         # --》1.sql如果执行异常，会回滚
    #         self.conn.rollback()
    #         print("该sql语句，在执行过程中出现问题,异常信息为：{errorMessage}".format(errorMessage=errorMessage))
    #     else:
    #         # 第2步：调用pymysql库提供的commit方法，通过执行存储在对象mysqlCur的sql
    #         self.conn.commit()
    #
    # # 以下代码块，等2021.07.15之后再进行优化编写
    #
    # def mysql_get_minimal_value_of_id(self):
    #     # return rows[0][0]
    #     pass
    #
    # def mysql_get_maximal_value_of_id(self, ):
    #     # return rows[0][0]
    #     pass
    #
    # def mysql_get_count_of_one_table_data(self, tableName="子子类赋值的这个表名"):
    #     # return rows[0][0]
    #     pass

    # def mysql_close(self):
    #     '''
    #     1.该接口作用：返回一个mysql数据库连接；
    #     '''
    #     conn = self.__getConnect()
    #     try:
    #         conn.close()
    #     except Exception as errorMessage:
    #         print("数据库关闭异常,异常信息为：{errorMessage}".format(errorMessage=errorMessage))
    #
    # def mysql_truncate_data(self, oneTableName_or_manyTableNames="子子类赋值的这个表名", enum=0):
    #
    #     '''
    #     :param oneTableName_or_manyTableNames；参数含义：一个数据表名或多个数据表名集合；数据类型为：str或者list；会给个默认值；
    #     :param enum；参数含义：枚举值；目前枚举值只设置3个；会给个默认值；
    #     1.接口作用： 清空一至多个表数据后，让自增id仍从1开始，数据表仍保留；
    #     classTest.测试数据demo，比如： tableNames = ["tp_banner","tp_cases","tp_books"]；
    #     '''
    #     if oneTableName_or_manyTableNames == "子子类赋值的这个表名" and enum == 0:
    #         sql = "truncate {tableName}".format(tableName=self.tableName)
    #         self.mysql_execute(sql)
    #     elif oneTableName_or_manyTableNames != "子子类赋值的这个表名" and enum == 1:
    #         if isinstance(oneTableName_or_manyTableNames, str):
    #             sql = "truncate {tableName}".format(tableName=oneTableName_or_manyTableNames)
    #             self.mysql_execute(sql)
    #     elif oneTableName_or_manyTableNames != "子子类赋值的这个表名" and enum == 2:
    #         if isinstance(oneTableName_or_manyTableNames, query_list):
    #             for i in oneTableName_or_manyTableNames:
    #                 sql = "truncate {tableName}".format(tableName=i)
    #                 self.mysql_execute(sql)
    #     else:
    #         raise Exception("入参值填写有误，请按照要求填写正确的tableName_or_sqlStatement和enum值！")
    #
    # #
    # ====================================================这些接口待改造================================================================================================
    #
    # def mysql_backups_data_of_tables(self, tableNames: query_list):
    #     '''
    #     :param tableNames； 参数含义：一至多个数据表的集合；数据类型为list；
    #     1.接口作用：生成一至多个数据表的备份，留当做每次执行一遍所有接口用例后生成的所有表数据的存档，跟开发人员讲相关bug时当证据；（删除/清空原始表数据之前，最好备份出一份新表当证据；）
    #     classTest.细节：有个备份数据表并重命名新备份表的sql语句，这个sql和相关代码后续再写,可咨询开发（新备份表的表名建议以精准到秒的时间当后缀名，例如：tp_banner20181129201020）
    #     3.备注：这个接口先暂时不实现功能；
    #     '''
    #     pass
    #
    # def cancelOneData(self, testcase):
    #     '''
    #     :param testcase  从excel文件获取到的单条接口数据，数据类型为dict
    #     接口作用： 删除某条数据表中已存在的数据(为了不干扰【更新接口】的接口测试用例的准确性)
    #     classTest.注意事项： 如果有其他新项目/新模块的接口跟这个接口内部逻辑不一样，可以在子类继承并重写该方法内的相关逻辑
    #     数字门店商品上下架.编写以下的范例脚本，不需调试，仅供参考，不用于其他途径
    #     :return None
    #     '''
    #     if testcase["表字段名"] == "字段名对应的具体值":
    #         self.mysql_execute("DELETE FROM 单个表名 WHERE 字段名 = 字段名对应的具体值")
    #     elif testcase["sort"] == 1:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = 造第二组仓库相关数据")
    #     elif testcase["sort"] == 99998:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = 99998")
    #     elif testcase["sort"] == 99999:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = 99999")
    #     elif testcase["sort"] == 100000:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = 100000")
    #     elif testcase["sort"] == -3:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = -数字门店商品上下架")
    #     elif testcase["sort"] == -6.yaml.6.yaml:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = -6.yaml.6.yaml")
    #     elif testcase["sort"] == 2.2:
    #         self.mysql_execute("DELETE FROM tp_banner WHERE sort = classTest.classTest")
    #
    # #
    # ==================================================================================================================================================================
    #
    #  ===以下接口都是为了实现新功能写的新接口，都调试通过；(从2020.04.04星期六开始写)
    # 注意：旧接口就保留原状，不要去动旧接口，因为旧接口可能被多个地方使用了；旧接口的改造或删除可以等后期有时间再处理；
    # 注意：如果要实现新功能，按照开发规范的正确处理方式，可以在旧接口里面改造，也可以直接写个新接口；

    # def get_oneRandomValue_or_allValues_of_one_tableField(self, a_designated_tableField_name: str,
    #                                                       specified_query_criteria=query_list, enum=0):
    #     '''
    #    :param tableName；参数含义：一个数据表名；数据类型为：str；
    #    :param a_designated_tableField_name 参数名：一个指定的表字段名；数据类型：str；
    #    :param specified_query_criteria 参数名：指定的查询条件；数据类型：query_list；比如值为：[["name","Tom"],["age",20]]；
    #                                    查询条件里支持嵌套零至多组list；每个list里的下标0对应下标值是一个表字段名A，下标1对应下标值是一个表字段名A的表字段值；
    #                                    目前最多嵌套4组list，后续可再优化；
    #    :param enum；参数含义：枚举值；目前枚举值只设置2个；会给个默认值0；
    #    :return
    #             enum枚举值为0表示"随机返回符合查询条件的一个默认数据表的一个表字段的一个值，值数据类型不固定"；
    #             enum枚举值为1表示"返回符合查询条件的一个默认数据表的一个表字段的所有值，值数据类型为list" ；
    #    '''
    #
    #     conn = self.__conn()
    #     cur = conn.cursor()
    #
    #     try:
    #         if len(specified_query_criteria) == 0:
    #             sql = "select %s from %s" % (a_designated_tableField_name, self.tableName)
    #         elif len(specified_query_criteria) == 1:
    #             sql = "select %s from %s where %s = '%s'" % (
    #                 a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
    #                 specified_query_criteria[0][1])
    #         elif len(specified_query_criteria) == 2:
    #             sql = "select %s from %s where %s ='%s' and %s = '%s'" % (
    #                 a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
    #                 specified_query_criteria[0][1], specified_query_criteria[1][0], specified_query_criteria[1][1])
    #         elif len(specified_query_criteria) == 3:
    #             sql = "select %s from %s where %s = '%s' and %s = '%s' and %s = '%s'" % (
    #                 a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
    #                 specified_query_criteria[0][1], specified_query_criteria[1][0], specified_query_criteria[1][1],
    #                 specified_query_criteria[2][0], specified_query_criteria[2][1])
    #         elif len(specified_query_criteria) == 4:
    #             sql = "select %s from %s where %s = '%s' and %s ='%s' and %s = '%s' and %s = '%s'" % (
    #                 a_designated_tableField_name, self.tableName, specified_query_criteria[0][0],
    #                 specified_query_criteria[0][1], specified_query_criteria[1][0], specified_query_criteria[1][1],
    #                 specified_query_criteria[2][0], specified_query_criteria[2][1], specified_query_criteria[3][0],
    #                 specified_query_criteria[3][1])
    #         # print(sql)
    #         cur.execute(sql)
    #
    #     except Exception as a:
    #         print("执行SQL语句出现异常：%s" % a)
    #     else:
    #         rows = cur.fetchall()
    #         cur.close()
    #         conn.close()
    #         # print(len(rows))
    #         # print(rows)
    #         emptyList = []
    #         for i in rows:
    #             emptyList.append(i[0])
    #         # print("111")
    #         # print(emptyList)
    #         # print("222")
    #         if enum == 0:
    #             value = random.choice(emptyList)
    #             return value
    #         elif enum == 1:
    #             value = emptyList
    #             return value


if __name__ == "__main__":
    from utils.get_env_variables import get_env_variables

    env_variables = get_env_variables()

    # 第1步:编写一个子类来继承父类MysqlUtils，并对属性mysql_database_connection_information进行赋值
    # 细节：
    # --》1.英文"subClass"的含义：子类
    # noinspection PyMissingOrEmptyDocstring
    class SubClass_of_MysqlUtils(MysqlUtils):
        mysql_database_connection_information = {"host": env_variables["petMysql_host"],
                                                 "port": env_variables["petMysql_port"],
                                                 "user": env_variables["petMysql_user"],
                                                 "passwd": env_variables["petMysql_passwd"],
                                                 "db": env_variables["petMysql_db"],
                                                 "charset": env_variables["petMysql_charset"],
                                                 }


    # 第2步：实例化子类，生成一个对象
    subClass_of_MysqlUtils = SubClass_of_MysqlUtils()

    # 第3步：通过对象调用方法query_of_sql
    subClass_of_MysqlUtils.query_of_sql("select * from sys_menu")
    subClass_of_MysqlUtils.query_of_sql("select label from sys_menu")
