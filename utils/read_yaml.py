# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月16日  11点41分
@contactInformation: 727803257@qq.com
@Function: 读取指定的单个yaml文件的数据
"""

# 请从这行往下开始编写脚本

import os
import sys
import yaml
from utils.getPath import GetPath

sys.path.append(os.getcwd())
getPath = GetPath()


def get_data_of_one_designated_yamlFile(secondFloorFileName="",
                                        thirdFloorFileName="",
                                        fourthFloorFileName="",
                                        fifthFloorFileName="",
                                        sixthFloorFileName="",
                                        seventhFloorFileName="",
                                        resourceName=""):
    """
    :param secondFloorFileName  第二层文件夹名，数据类型为str
    :param thirdFloorFileName   第三层文件夹名，数据类型为str
    :param fourthFloorFileName  第四层文件夹名，数据类型为str
    :param fifthFloorFileName   第五层文件夹名，数据类型为str
    :param sixthFloorFileName   第六层文件夹名，数据类型为str
    :param seventhFloorFileName 第七层文件夹名，数据类型为str
    :param resourceName         要获取的单个yaml文件名，数据类型为str
    :return 读取指定的单个yaml文件的数据(对数据的数据类型不做任何改变)，数据类型为dict
    """

    # 第1步：获取这个yaml文件的绝对路径
    yaml_path = getPath.get_absolute_path_of_one_resource(secondFloorFileName=secondFloorFileName,
                                                          thirdFloorFileName=thirdFloorFileName,
                                                          fourthFloorFileName=fourthFloorFileName,
                                                          fifthFloorFileName=fifthFloorFileName,
                                                          sixthFloorFileName=sixthFloorFileName,
                                                          seventhFloorFileName=seventhFloorFileName,
                                                          resourceName=resourceName)

    # 第2步：通过open方法打开这个yaml文件，并返回存储这个yaml文件所有数据的对象f
    f = open(yaml_path, encoding="utf-8")

    # 第3步：通过load方法读取对象f，并返回存储这个yaml文件所有数据的数据类型为dict的变量data
    data = yaml.load(f.read(), Loader=yaml.SafeLoader)

    # 第4步：返回变量data
    return data


if __name__ == '__main__':
    get_data_of_one_designated_yamlFile(secondFloorFileName="config", resourceName="config")
