# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年04月16日  14点31分
@contactInformation: 727803257@qq.com
@Function: 实现连接一个指定的redis数据库，并执行相关操作
"""

# 请从这行往下开始编写脚本

import redis


class RedisUtils:
    """实现连接一个指定的redis数据库，并执行相关操作"""

    # 第1步：设置一个属性redis_database_connection_information，用于存储redis数据库登录账号信息
    # 细节：
    # --》1.属性redis_database_connection_information的含义：redis数据库登录账号信息
    # --》2.属性redis_database_connection_information的数据类型：dict
    # --》3.属性redis_database_connection_information的值必须只能在子类里被重写(被重写后的值必须是正确的redis数据库登录账号信息)
    redis_database_connection_information = {}

    # 细节：
    # --》1.方法"__init__"在父类或子类被实例化后才会被执行，且执行优先级低于父类或子类里的属性
    def __init__(self):
        # 细节：
        # --》1.参数conn的含义：redis数据库连接池对象(连接池对象可以理解为是一条高速公路)
        # 第1步：得到一个redis数据库连接池对象
        self.conn = redis.StrictRedis(host=self.redis_database_connection_information['host'],
                                      port=self.redis_database_connection_information['port'],
                                      password=self.redis_database_connection_information['password'],
                                      )

    def get_allKeys(self):
        """
        :return allKeys  redis数据库里的所有key，数据类型为list
        # 细节：
        # --》1.1.每个key的数据类型为byte
        # --》1.2.比如某个key的具体值为：b'LOCAL:ZGBZ_SUPPLY:LOGIN:STORE_INFO:SINGLE:2020121513190793700000001'
        """
        # 第1步：调用redis库提供的方法，得到redis数据库里的所有key
        allKeys = self.conn.keys()
        # 第2步：返回allKeys
        return allKeys

    def set_value_of_a_key(self, key, value):
        """
        :param key      redis数据库里的一个key，数据类型为byte
        :param value    redis数据库里的一个指定key对应的value，数据类型为byte
        :return None
        # 细节：
        # --》1.1.比如某个key的具体值为：b'LOCAL:ZGBZ_SUPPLY:LOGIN:STORE_INFO:SINGLE:2020121513190793700000001'
        # --》1.2.比如某个key对应的value的具体值为：b'3306'
        """
        # 第1步：调用redis库提供的方法，在redis数据库里修改一个指定key对应的value(如果这个指定的key不存在，则新增这个指定的key和对应的value)
        self.conn.set(name=key, value=value)

    def get_value_of_a_key(self, key):
        """
        :param key     redis数据库里的一个指定key，数据类型为byte
        :return value  redis数据库里的一个指定key对应的value，数据类型为byte
        # 细节：
        # --》1.1.比如某个key的具体值为：b'LOCAL:ZGBZ_SUPPLY:LOGIN:STORE_INFO:SINGLE:2020121513190793700000001'
        # --》1.2.比如某个key对应的value的具体值为：b'3306'
        """
        # 第1步：调用redis库提供的方法，得到redis数据库里的一个指定key对应的value
        value = self.conn.get(key)
        # 第2步：返回value
        return value

    def get_expiration_time(self, key):
        """
        :param key  redis数据库里的一个指定key，数据类型为byte
        :return expiration_time  redis数据库里的一个指定key对应的过期时间，数据类型为int
        # 细节：
        # --》1.1.比如某个key的具体值为：b'LOCAL:ZGBZ_SUPPLY:LOGIN:STORE_INFO:SINGLE:2020121513190793700000001'
        # --》1.2.比如某个key对应的过期时间的具体值为：200
        # --》1.3.过期时间的单位为秒
        # --》1.4.我们可以设置每个key的过期时间
        # --》1.5.当一个key的当前系统大于设置的过期时间，那么这个key就会被销毁，我们再也搜不到这个key
        # --》2.英文"expiration"的含义：过期
        # --》3.英文"expiration_time"的含义：过期时间
        """
        # 第1步：调用redis库提供的方法，得到redis数据库里的一个指定key对应的过期时间
        expiration_time = self.conn.ttl(key)
        # 第2步：返回expiration_time
        return expiration_time


if __name__ == "__main__":
    from utils.get_env_variables import get_env_variables

    env_variables = get_env_variables()

    # 第1步:编写一个子类来继承父类RedisUtils，并对属性redis_database_connection_information进行赋值
    # 细节：
    # --》1.英文"subClass"的含义：子类
    # noinspection PyMissingOrEmptyDocstring
    class SubClass_of_RedisUtils(RedisUtils):
        redis_database_connection_information = {"host": env_variables["petRedis_host"],
                                                 "port": env_variables["petRedis_port"],
                                                 "password": env_variables["petRedis_password"],
                                                 }


    # 第2步：实例化子类，生成一个对象
    subClass_of_RedisUtils = SubClass_of_RedisUtils()

    # 通过对象调用方法get_allKeys
    subClass_of_RedisUtils.get_allKeys()

    # 通过对象调用方法set_value_of_a_key，生成一个新的key(值为b2)和对应的value(值为"10086")
    # 细节：
    # --》1.生成的数据，可以通过在redis客户端进行查看
    subClass_of_RedisUtils.set_value_of_a_key(key="b3".encode(), value="10086".encode())

    # 通过对象再次调用方法set_value_of_a_key，更改key(值为b2)的新value
    # 细节：
    # --》1.更新的数据，可以通过在redis客户端进行查看
    subClass_of_RedisUtils.set_value_of_a_key(key="b3", value="0000".encode())

    # 通过对象调用方法get_value_of_a_key，获取key(值为b2)的value
    subClass_of_RedisUtils.get_value_of_a_key(key="b3".encode())

    # 通过对象调用方法get_expiration_time
    subClass_of_RedisUtils.get_expiration_time(key="b3".encode())

    # 新增一条数据，用于测试相关接口测试用例
    subClass_of_RedisUtils.set_value_of_a_key(key="b2_aaa_bbb_ccc".encode(), value="0000".encode())
