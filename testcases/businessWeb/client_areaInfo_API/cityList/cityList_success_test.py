# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月07日  10点37分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""

# 请从这行往下开始编写脚本
import allure
import pytest
from httprunner import Parameters
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


@allure.epic("宜买车项目-用户web端")
@allure.feature("客户端地区信息api")
@allure.story("售车城市列表")
@pytest.mark.smoke
# @pytest.mark.skip("正确的access_token目前只能通过手工获取，所以这条冒烟用例在自动化持续继承时不参与执行")
class TestCaseCityListSuccess(HttpRunner):

    config = (
        Config("售车城市列表-获取信息成功")
            .base_url("${ENV(baseUrlOfBusinessWeb)}")
            .variables(**{"access_token":"4-545324bf-c68c-4435-a1e3-78583c6c6f55"})
            .verify(True)
            .export(*[])
    )

    teststeps = [

        Step(
            RunRequest("售车城市列表-获取信息成功")
                .get("/client/areaInfo/city/list")
                .with_headers(**{"authorization": "Bearer " + "$access_token"})
                .with_params(
                **{}

            )
                .validate()
                .assert_equal("status_code", 200)
                .assert_equal("body.message", "success")
        )
    ]


if __name__ == "__main__":
    TestCaseCityListSuccess().test_start()
