# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月06日  21点10分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""

# 请从这行往下开始编写脚本

import allure
import pytest
from httprunner import Parameters
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase
from testcases.businessWeb.client_login_API.getGraphValidateCode.getGraphValidateCode_success_test \
    import TestCaseGetGraphValidateCode as GetGraphValidateCode


@allure.epic("宜买车项目-用户web端")
@allure.feature("客户端登录api")
@allure.story("获取短信验证码")
class TestCaseSendMsgGraphCode(HttpRunner):

    @pytest.mark.parametrize("param", Parameters({
        "mobile-graphCode-code-message": [
            ["17750622380", "", 24001, "图形验证码不能为空"],
            ["17750622380", "77aa", 21000210, "请填写正确的图形验证码"],

        ]
    }))
    def test_start(self, param):
        super().test_start(param)

    config = (
        Config("获取短信验证码-针对入参graphCode的校验")
            .base_url("${ENV(baseUrlOfBusinessWeb)}")
            .variables(**{})
            .verify(True)
            .export(*[])
    )

    teststeps = [
        Step(
            RunTestCase("获取图形验证码-成功")
                .call(GetGraphValidateCode)
                .export(*["graphKey", "graphImage"])
        ),


        Step(
            RunRequest("获取短信验证码-针对入参graphCode的校验")
                .post("/client/login/sendMsg")
                .with_headers(**{})
                .with_json(
                {
                    "graphCode": "$graphCode",
                    "graphKey": "$graphKey",
                    "mobile": "$mobile",
                }

            )
                .validate()
                .assert_equal("status_code", 200)
                .assert_equal("body.code", "$code")
                .assert_equal("body.message", "$message")
        )
    ]


if __name__ == "__main__":
    TestCaseSendMsgGraphCode().test_start()
