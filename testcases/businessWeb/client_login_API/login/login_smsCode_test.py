# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月06日  21点31分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""

# 请从这行往下开始编写脚本

import allure
import pytest
from httprunner import Parameters
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


@allure.epic("宜买车项目-用户web端")
@allure.feature("客户端登录api")
@allure.story("登录")
class TestCaseLoginSmsCode(HttpRunner):

    @pytest.mark.parametrize("param", Parameters({
        "cityCode-mobile-smsCode-code-message": [
            [350200, "17750622380", "", 24001, "短信验证码不能为空"],
            [350200, "17750622380", "111222", 2100021, "短信验证码填写错误"],

        ]
    }))
    def test_start(self, param):
        super().test_start(param)

    config = (
        Config("登录-针对入参smsCode的校验")
            .base_url("${ENV(baseUrlOfBusinessWeb)}")
            .variables(**{})
            .verify(True)
            .export(*[])
    )

    teststeps = [

        Step(
            RunRequest("登录-针对入参smsCode的校验")
                .post("/client/login/login")
                .with_headers(**{})
                .with_json(
                {
                    "cityCode": "$cityCode",
                    "mobile": "$mobile",
                    "smsCode": "$smsCode",
                }

            )
            .validate()
            .assert_equal("status_code", 200)
            .assert_equal("body.code", "$code")
            .assert_equal("body.message", "$message")
        )
    ]


if __name__ == "__main__":
    TestCaseLoginSmsCode().test_start()
