# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月06日  19点47分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""

# 请从这行往下开始编写脚本
import pytest
import allure
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


@allure.epic("宜买车项目-用户web端")
@allure.feature("客户端登录api")
@allure.story("获取图形验证码")
@pytest.mark.smoke
@pytest.mark.formal
class TestCaseGetGraphValidateCode(HttpRunner):
    config = (
        Config("获取图形验证码-成功")
            .base_url("${ENV(baseUrlOfBusinessWeb)}")
            .variables(**{})
            .verify(True)
            .export(*["graphKey", "graphImage"])
    )

    teststeps = [

        Step(
            RunRequest("获取图形验证码-成功")
                .get("/client/login/getGraphValidateCode")
                .with_headers(**{})
                .with_params(**{})
                .extract()
                .with_jmespath("body.data.graphKey", "graphKey")
                .with_jmespath("body.data.image", "graphImage")
                .validate()
                .assert_equal("status_code", 200)
                .assert_equal("body.message", "success")
                .assert_length_equal("body.data.graphKey", 6)
        )
    ]


if __name__ == "__main__":
    TestCaseGetGraphValidateCode().test_start()
