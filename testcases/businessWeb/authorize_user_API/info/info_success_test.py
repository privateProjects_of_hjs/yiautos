# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月07日  09点58分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""
# 请从这行往下开始编写脚本

import allure
import pytest
from httprunner import Parameters
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


@allure.epic("宜买车项目-用户web端")
@allure.feature("买家信息api")
@allure.story("买家个人信息")
@pytest.mark.smoke
@pytest.mark.formal
# @pytest.mark.skip("正确的access_token目前只能通过手工获取，所以这条冒烟用例在自动化持续继承时不参与执行")
class TestCaseInfoSuccess(HttpRunner):

    @pytest.mark.parametrize("param", Parameters({
        "access_token-mobile": [
            ["4-545324bf-c68c-4435-a1e3-78583c6c6f55", "17750622380"],

        ]
    }))
    def test_start(self, param):
        super().test_start(param)

    config = (
        Config("买家个人信息-获取买家个人信息成功")
            .base_url("${ENV(baseUrlOfBusinessWeb)}")
            .variables(**{})
            .verify(True)
            .export(*[])
    )

    teststeps = [

        Step(
            RunRequest("买家个人信息-针对入参access_token的校验")
                .get("/authorize/user/info")
                .with_headers(**{})
                .with_params(
                **{
                    "access_token": "$access_token",
                }

            )
            .validate()
            .assert_equal("status_code", 200)
            .assert_length_equal("body.principal.id", 32)
            .assert_equal("body.principal.mobile", "$mobile")
        )
    ]


if __name__ == "__main__":
    TestCaseInfoSuccess().test_start()
