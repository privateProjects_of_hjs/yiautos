# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月06日  21点31分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""

# 请从这行往下开始编写脚本

import allure
import pytest
from httprunner import Parameters
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


@allure.epic("宜买车项目-用户web端")
@allure.feature("买家信息api")
@allure.story("买家个人信息")
class TestCaseInfoAccessToken(HttpRunner):

    @pytest.mark.parametrize("param", Parameters({
        "access_token-status_code-error-error_description": [
            ["",401, "invalid_token",""],
            ["4-3c62dbb2-01da-4253-ae6a-7098ff666666",401, "invalid_token","4-3c62dbb2-01da-4253-ae6a-7098ff666666"],

        ]
    }))
    def test_start(self, param):
        super().test_start(param)

    config = (
        Config("买家个人信息-针对入参access_token的校验")
            .base_url("${ENV(baseUrlOfBusinessWeb)}")
            .variables(**{})
            .verify(True)
            .export(*[])
    )

    teststeps = [

        Step(
            RunRequest("买家个人信息-针对入参access_token的校验")
                .get("/authorize/user/info")
                .with_headers(**{})
                .with_params(
                **{
                    "access_token": "$access_token",
                }

            )
            .validate()
            .assert_equal("status_code", "$status_code")
            .assert_equal("body.error", "$error")
            .assert_equal("body.error_description", "$error_description")
        )
    ]


if __name__ == "__main__":
    TestCaseInfoAccessToken().test_start()
