# coding:utf-8
"""
@author: jingsheng hong
@ide: PyCharm
@createTime: 2021年11月06日  21点47分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：数据库语句代码操作)
"""

# 请从这行往下开始编写脚本

import allure
import pytest
from httprunner import Parameters
from httprunner import HttpRunner, Config, Step, RunRequest, RunTestCase


@allure.epic("宜买车项目-底层权限端")
@allure.feature("权限api")
@allure.story("获取accessToken")
class TestCaseTokenAuthorization(HttpRunner):

    @pytest.mark.parametrize("param", Parameters({
        "authorization-status_code-error": [
            ["", 401, "Unauthorized"],
            ["Basic Y3JtLWNsaWVudDokMmEkMTAkaHYxWk5vbnRyWnI4cHRxVlA2TGhQZWtjSWF0a3NhZUJaZDNpUmwxUE0=", 400,
             "invalid_request"],

        ]
    }))
    def test_start(self, param):
        super().test_start(param)

    config = (
        Config("获取accessToken-针对请求头参数authorization的校验")
            .base_url("${ENV(baseUrlOfOauth)}")
            .variables(**{})
            .verify(True)
            .export(*[])
    )

    teststeps = [

        Step(
            RunRequest("获取accessToken-针对请求头参数authorization的校验")
                .post("/token")
                .with_headers(**{"authorization": "$authorization", })
                .with_json(

                    {"type": "4",
                    "grant_type": "password",
                    "username": "17750622380",
                    "password": "5aaee0c823974c188fb28e90f29817f7",
                    "scope": "trust"}


            )
                .validate()
                .assert_equal("status_code", "$status_code")
                .assert_equal("body.error", "$error")
        )
    ]


if __name__ == "__main__":
    TestCaseTokenAuthorization().test_start()
