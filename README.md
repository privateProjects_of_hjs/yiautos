# 细节：
# -->1.在pycharm里通过新建一个名为【README.md】的md文件格式的文件就能新建成功【README.md】这个文件。
# -->2.【README.md】的知识点，可以百度这个链接：https://www.baidu.com/s?wd=README.md。
# -->3.针对这个项目的所有介绍信息都可以写在下面。

# 因为我还没学Markdown语言，所以暂时就按txt格式进行编写了。


# 1.生成这个项目依赖的所有库信息的命令行为：
#   pip freeze >requirements.txt  
#   注意事项：一、这条命令行要在项目根目录下执行；
#           二、执行成功这条命令行后，会在项目根目录下生成一个包含这个项目依赖的所有库信息的【requirements.txt】

# 2.要在其他电脑(比如服务器)上安装这个项目的依赖的所有库，可以执行这条命令行：
#   pip install –r requirements.txt  
#   注意事项：一、执行成功这条命令行后，会成功安装这个项目的依赖的所有库

# 3.这个项目的作用：
#   实现宜买车项目所有应用端的接口单元测试和接口自动化测试。

# 4.执行哪个环境的测试用例之前，一定要先更新环境配置数据!
#   4.1.比如要切换为测试环境的环境配置数据的命令行为：
#   python read_env.py envs/test.env
#   4.2.比如要切换为uat环境的环境配置数据的命令行为：
#   python read_env.py envs/uat.env
#   4.3.比如要切换为开发环境的环境配置数据的命令行为：
#   python read_env.py envs/dev.env
#   4.4.比如要切换为正式环境的环境配置数据的命令行为：
#   python read_env.py envs/formal.env


# 5.项目的所有接口用例都放在项目根目录的二级目录【testcases】里。
# 5.1.要执行二级目录【testcases】里的所有的接口用例的参考的命令行为：
#  E:\privateProjects_of_hjs\petProject\testcases>pytest
# 5.2.要执行二级目录【testcases】里的某个api模块里的所有的接口用例的参考的命令行为：
#  E:\privateProjects_of_hjs\petProject\testcases\operate\common_captcha_API>pytest


# 6.生成html报告的相关命令(如果遗忘了话，可以参考校长的博客或者ppt)
# 6.1.要执行二级目录【testcases】里的所有的接口用例后生成html报告的参考的命令行为：
#  E:\privateProjects_of_hjs\petProject\testcases>pytest --html=./report/result.html --self-contained-html(这个后续就不要用了，都统一要从项目根目录开始执行，这样生成的报告才会在根目录，方便在jenkns里被获取)
# 或者(因为所有接口用例都只放在二级目录【testcases】里，所以从项目根目录开始执行也是可以的也是能实现同样效果):
#  E:\privateProjects_of_hjs\petProject>pytest --html=./report/result.html --self-contained-html(强烈推荐这个用法)
#  E:\privateProjects_of_hjs\petProject>pytest testcases --html=./report/result.html --self-contained-html(强烈推荐这个用法)

# 6.2.要执行二级目录【testcases】里的某个api模块里的所有的接口用例后生成html报告的参考的命令行为：
#  E:\privateProjects_of_hjs\petProject>pytest testcases/operate/common_captcha_API --html=./report/result.html --self-contained-html(强烈推荐这个用法)

# 6.3.要执行二级目录【testcases】里的某个api模块里的某个接口的所有接口用例后生成html报告的参考的命令行为：
#  E:\privateProjects_of_hjs\petProject>pytest testcases/operate/common_captcha_API/checkCaptcha --html=./report/result.html --self-contained-html(强烈推荐这个用法)

# 6.4.要执行二级目录【testcases】里的某个api模块里的某个接口里的某个脚本的接口用例后生成html报告的参考的命令行为：
#  E:\privateProjects_of_hjs\petProject>pytest testcases/operate/common_captcha_API/checkCaptcha/checkCaptcha_capText_001_test.py --html=./report/result.html --self-contained-html(强烈推荐这个用法)

# 7.生成allure报告的相关命令(跟生成html报告的相关命令差别不大)
# 7.1.第一步：执行指定测试用例，并生成一个二级目录【allure_report】并在二级目录【allure_report】下生成json格式的报告文件
#     参考的命令行为：E:\privateProjects_of_hjs\petProject>pytest testcases --alluredir ./allure_report (这四条命令行实现的效果一样：执行testcases二级目录里的所有测试用例)
#     参考的命令行为：E:\privateProjects_of_hjs\petProject>pytest -v testcases --alluredir ./allure_report (这四条命令行实现的效果一样：执行testcases二级目录里的所有测试用例)
#     参考的命令行为：E:\privateProjects_of_hjs\petProject>pytest  --alluredir ./allure_report(这四条命令行实现的效果一样：执行项目根目录里的所有测试用例)
#     参考的命令行为：E:\privateProjects_of_hjs\petProject>pytest -v --alluredir ./allure_report(这四条命令行实现的效果一样：执行项目根目录里的所有测试用例)
#     细节：因为项目的所有测试用例都存储在testcases二级目录里，所以执行项目根目录里的所有测试用例，其实就是执行testcases二级目录里的所有测试用例
# 7.2.第二步：运行后会在当前目录生成allure_report文件夹，使用allure目录行工具启动服务
#     参考的命令行为：allure serve ./allure_report

# 这条命令行的含义是： 执行标签名为smoke的用例，并按显示标准输出所有详情信息，并把数据存储在已清空的在项目的二级空文件夹allure_report里(避免之前的测试数据的干扰)
# -v的作用：详细输出所有内容
# -s的作用：显示标准输出(包含代码里写的任何print和log信息)（如果要在生成的allure报告里有所有日志log，命令行里不能有"-s"这个命令，我已经亲自调试通过了）
# 用命令行，能够方便执行，还能完美跟jenkins结合！
# pytest -v -m smoke --alluredir ./allure_report --clean-alluredir （第一步：先执行用例并生成数据类型为json的测试报告!后续都参考这个命令行就行了！）
# allure serve ./allure_report（使用allure目录行工具启动服务，生成网页版的allure报告）
# 参考网站1：https://blog.csdn.net/Anthea_1013/article/details/118340925?spm=1001.2101.3001.6650.1&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.no_search_link&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.no_search_link
# 参考网站2：https://blog.csdn.net/minzhung/article/details/103333853
# 参考网站3：https://blog.csdn.net/weixin_45579030/article/details/109745440
# 备注:如果要匹配多个标签的组合条件，标签内容要用英文格式的双引号标记起来，命令行才能被正确执行
# pytest -v -m "smoke and formal" --alluredir ./allure_report --clean-alluredir 

# 性能测试的相关命令行
# 1.执行指定的一个接口冒烟用例：(venv) E:\privateProjects_of_hjs\yiautos\testcases\businessWeb\client_login_API\getGraphValidateCode>locusts -f getGraphValidateCode_success_test.py -H https://api.erp.yiautos.com --web-host="127.0.0.1"
