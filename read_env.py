# coding:utf-8
"""
@author: hjs
@ide: PyCharm
@createTime: 2021年10月15日  13点55分
@contactInformation: 727803257@qq.com
@Function: 请描述这个py文件的作用(范例：操作数据库来进行数据的存储更新)
"""

# 请从这行往下开始编写脚本
import os
import sys


def read_env(filepath):
    """

    :param filepath: 存储指定一套环境的所有环境变量的文件地址
    """
    current_path = os.path.abspath(os.path.dirname(__file__))
    print("current_path:", current_path)
    re_data = os.path.join(current_path, filepath)
    env_file = os.path.join(current_path, '.env')
    print("当前指定的要替换的env：", re_data)
    print("当前指定的要被替换的env：", env_file)
    data_renew = ""
    with open(re_data, "r", encoding="utf-8") as ff:
        for line in ff:
            line = line.strip()
            if not len(line) or line.startswith('#'):
                continue
            else:
                data_renew += line + '\n'
    with open(env_file, 'w', encoding="utf-8") as f:
        f.write(data_renew)
    print(data_renew)


if __name__ == "__main__":
    # 用于jenkins读取参数执行
    path = sys.argv[1]
    read_env(path)
